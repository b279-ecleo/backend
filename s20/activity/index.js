// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let i = 0; i < string.length; i++) {
  let letter = string[i].toLowerCase();

  if(letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u") {
    continue;
  } else {
    filteredString += letter;
  }
}

console.log(filteredString);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}