const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const productController = require("../Controllers/productController");

// CREATE PRODUCT
router.post("/Create", (req, res) => {
    let data = {
      userId: auth.decode(req.headers.authorization).id,
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      product: req.body
    };
    productController.createProduct(data).then((resultFromController) => res.send(resultFromController));
  });

// GET ALL PRODUCTS
router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
})
// GET ALL ACTIVE PRODUCTS
router.get("/allactive", (req, res) => {
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController));
})
// GET SPECIFIC PRODUCT
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Edit specific product
router.put("/:productId/update", auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  productController.updateProduct(req.params, req.body, isAdmin)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => {
      console.error(error);
      res.status(500).send("An error occurred while updating the product.");
    });
});

// Route to archiving a course
router.put("/:productId/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.archiveProduct(req.params,req.body,isAdmin).then(resultFromController => res.send(resultFromController));

});

router.put("/:productId/search", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.searchProduct(req.params.productId, isAdmin)
      .then(resultFromController => res.send(resultFromController))
      .catch(error => res.status(500).send(error));
  });
  
module.exports = router;