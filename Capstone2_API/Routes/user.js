const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const userController = require("../Controllers/userController");

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for to get all user
router.get("/alluser", (req, res) => {
	userController.getAllUser(req.body).then(resultFromController => res.send(resultFromController));
});
// Route to update user to admin by admin only
router.put("/:userId/Update", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.updateAdmin(req.params,req.body,isAdmin).then(resultFromController => res.send(resultFromController));

});
  
// ROUTE TO GET SPECIFIC ID
router.get("/:userId", (req, res) => {
	console.log(req.params.userId);
	userController.getSpecificUser(req.params).then(resultFromController => res.send(resultFromController));

})
//ROUTE TO UPDATE USERS INFO
router.put("/:userId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.updateUser(req.params, isAdmin, req.body).then(resultFromController => res.send(resultFromController));
	
});
// CHECKOUT
router.post("/checkout", auth.verify, (req, res) => {
	const data = {
	  isAdmin: auth.decode(req.headers.authorization).isAdmin,
	  userId: auth.decode(req.headers.authorization).id,
	  product: req.body
	};
	const reqBody = req.body; // Pass the reqBody argument
	const isAdmin = auth.decode(req.headers.authorization).isAdmin; // Pass the isAdmin argument
  
	userController.checkout(data, reqBody, isAdmin)
	  .then(resultFromController => res.send(resultFromController))
	  .catch(error => res.status(500).send("Something went wrong with your request. Please try again later!"));
  });
  

router.get("/order", (req, res) => {
	userController.getAllOrder().then(resultFromController => res.send(resultFromController));
})
// CHECK EMAIL
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});
module.exports = router;