// product.js
const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
  productBrand: {
    type: String,
  },
  productName: {
    type: String,
  },
  description: {
    type: String,
  },
  price: {
    type: Number,
  },
  stocks: {
    type: Number,
  },
  imgSource: {
    type: String,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  userOrders: [
    {
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
      },
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Product", productSchema);