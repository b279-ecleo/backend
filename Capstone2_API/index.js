// Server creation and Database Connection
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/user");
const productRoutes = require("./Routes/product");
const app = express();

//MongoDB Connection SRV
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.gphzc73.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//Optional - Validation of DB Connection
mongoose.connection.once("open", () => console.log("Successfully conencted to the server"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" to be included for all user routes defined in the "user" route file
app.use("/user", userRoutes);
app.use("/product", productRoutes);



// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4420, () => console.log(`API is now online on port ${process.env.PORT || 4420}`));
}

module.exports = app;