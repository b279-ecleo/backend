db.fruits.count({onSale: true})

db.fruits.count({stock: {$gte: 20}})

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}}
])

db.fruits.aggregate([
    {$group: {_id: {supplier: "$supplier_id", fruit: "$name"}, maxPrice: {$max: "$price"}}},
    {$group: {_id: "$_id.supplier", maxPrices: {$push: {fruit: "$_id.fruit", price: "$maxPrice"}}}}
])

db.fruits.aggregate([
    {$group: {_id: {supplier: "$supplier_id", fruit: "$name"}, minPrice: {$min: "$price"}}},
    {$group: {_id: "$_id.supplier", minPrices: {$push: {fruit: "$_id.fruit", price: "$minPrice"}}}}
])
