db.rooms.insertOne({
  name: "single",
  accommodates: 2,
  price: 1000,
  description: "A simple room with all the basic necessities",
  rooms_available: 10,
  isAvailable: false
})

db.rooms.insertMany([
  {
    name: "queen",
    accommodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for simple getaway",
    rooms_available: 15,
    isAvailable: false
  },
  {
    name: "King",
    accommodates: 5,
    price: 3000,
    description: "A room with a king sized bed perfect for simple getaway",
    rooms_available: 12,
    isAvailable: false
  },

  {
    name: "Double",
    accommodates: 6,
    price: 3000,
    description: "A room with a double bed perfect for simple getaway",
    rooms_available: 12,
    isAvailable: false
  }
]);

db.rooms.find({name: "Double"});

db.rooms.updateOne(
  { name: "queen" }, 
  { $set: { rooms_available: 0 } }
);

db.rooms.deleteMany({
	rooms_available: 12,
});