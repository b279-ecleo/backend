const express = require ("express");

// Create an application using express
// In layman's term, app is our server

const app = express();
const port = 4000;

// Setup for allowing server to handle data
// Allows your app to read json data
// Methods used from express are middlewares
// Middlewares -> provides common services and capabilities of the app

app.use(express.json());
app.use(express.urlencoded({extended:true}));